#include <iostream>
#include <vector>
#include <limits>
#include <cassert>

namespace
{

int solvePigbank(int weight, std::vector<int> coins, std::vector<int> coins_weight)
{
    assert(weight >= 0);
    assert(coins.size() == coins_weight.size());

    std::vector<int> dp(weight + 1, std::numeric_limits<int>::max());
    dp[0] = 0;

    for (auto i = 0; i < dp.size(); ++i)
        for (auto j = 0; j < coins.size(); ++j)
        {
            auto coin_weight = coins_weight[j];
            if (i - coin_weight >= 0
                and dp[i - coin_weight] != std::numeric_limits<int>::max()
                and dp[i - coin_weight] + coins[j] < dp[i])
                dp[i] = dp[i - coin_weight] + coins[j];
        }

    return dp.back();
}

void solveAndDescribePigbank(int weight, std::vector<int> coins, std::vector<int> coins_weight)
{
    auto result = solvePigbank(weight, coins, coins_weight);
    if (result == std::numeric_limits<int>::max())
        std::cout << "This is impossible." << std::endl;
    else
        std::cout << "The minimum amount of money in the piggy-bank is " << result << "." << std::endl;
}

} // namespace

int main()
{
    std::cin.sync_with_stdio(false);
    std::cout.sync_with_stdio(false);
    std::cerr.sync_with_stdio(false);

    int T;
    std::cin >> T;
    for (auto i = 0; i < T; ++i)
    {
        int E, F;
        std::cin >> E >> F;
        int N;
        std::cin >> N;
        std::vector<int> coins;
        std::vector<int> coins_weight;
        for (auto j = 0; j < N; ++j)
        {
            int P, W;
            std::cin >> P >> W;
            coins.push_back(P);
            coins_weight.push_back(W);
        }
        solveAndDescribePigbank(F - E, coins, coins_weight);
    }

//    solveAndDescribePigbank(110 - 10, {1, 30}, {1, 50});
//    solveAndDescribePigbank(110 - 10, {1, 50}, {1, 30});
//    solveAndDescribePigbank(6 - 1, {10, 3}, {20, 4});

    return 0;
}

