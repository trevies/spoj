pigbank:
	g++ --std=c++14 -O3 pigbank.cpp -o pigbank
	./pigbank < pigbank_in

clean_pigbank:
	rm -f pigbank


all: pigbank

clean: clean_pigbank
